# CloudStream

# `⚠️ Warning: `
**I'm not the original developer.**
**The app in [releases](https://codeberg.org/Stormunblessed/cloudstream/releases) will be signed by myself.**

**This application does not contain any extensions by default, you will have to search the internet or create them yourself, I am not responsible for the content that these extensions may have.**

**The link below is an invitation to the original dev discord**
[![Discord](https://invidget.switchblade.xyz/5Hus6fM)](https://discord.gg/5Hus6fM)

### Features:
+ **AdFree**, No ads whatsoever
+ No tracking/analytics
+ Bookmarks
+ Download and stream movies, tv-shows and anime
+ Chromecast

### Supported languages:
<a href="https://hosted.weblate.org/engage/cloudstream/">
  <img src="https://hosted.weblate.org/widgets/cloudstream/-/app/multi-auto.svg" alt="Translation status" />
</a>
 